﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShadyRaysPOC.Barcode.Services.ShipEngine.Models
{
    public class ShipOrderModel : ShipEngineResponse
    {
        [JsonProperty("shipment_id")]
        public string ShipmentId { get; set; }

        [JsonProperty("carrier_id")]
        public string CarrierId { get; set; }

        [JsonProperty("service_code")]
        public string ServiceCode { get; set; }

        [JsonProperty("external_shipment_id")]
        public string ExternalShipmentId { get; set; }

        [JsonProperty("ship_date")]
        public DateTime? ShipDate { get; set; }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty("modified_at")]
        public DateTime? ModifiedAt { get; set; }

        [JsonProperty("shipment_status")]
        public string ShipmentStatus { get; set; }

        [JsonProperty("ship_to")]
        public ShipTo ShipTo { get; set; }

        [JsonProperty("ship_from")]
        public ShipFrom ShipFrom { get; set; }

        [JsonProperty("warehouse_id")]
        public string WarehouseId { get; set; }

        [JsonProperty("return_to")]
        public ReturnTo ReturnTo { get; set; }

        [JsonProperty("confirmation")]
        public string Confirmation { get; set; }

        [JsonProperty("customs")]
        public string Customs { get; set; }

        [JsonProperty("external_order_id")]
        public string ExternalOrderId { get; set; }

        [JsonProperty("order_source_code")]
        public string OrderSourceCode { get; set; }

        [JsonProperty("advanced_options")]
        public AdvancedOptions AdvancedOptions { get; set; }

        [JsonProperty("insurance_provider")]
        public string InsuranceProvider { get; set; }

        [JsonProperty("packages")]
        public List<Package> Packages { get; set; }

        [JsonProperty("total_weight")]
        public TotalWeight TotalWeight { get; set; }

        public ShipOrderModel() { }
    }

    public class ShipTo
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone")]
        public object Phone { get; set; }

        [JsonProperty("company_name")]
        public object CompanyName { get; set; }

        [JsonProperty("address_line1")]
        public string AddressLine1 { get; set; }

        [JsonProperty("address_line2")]
        public object AddressLine2 { get; set; }

        [JsonProperty("address_line3")]
        public object AddressLine3 { get; set; }

        [JsonProperty("city_locality")]
        public string CityLocality { get; set; }

        [JsonProperty("state_province")]
        public string StateProvince { get; set; }

        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("address_residential_indicator")]
        public string AddressResidentialIndicator { get; set; }
    }

    public class ShipFrom
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("company_name")]
        public string CompanyName { get; set; }

        [JsonProperty("address_line1")]
        public string AddressLine1 { get; set; }

        [JsonProperty("address_line2")]
        public object AddressLine2 { get; set; }

        [JsonProperty("address_line3")]
        public object AddressLine3 { get; set; }

        [JsonProperty("city_locality")]
        public string CityLocality { get; set; }

        [JsonProperty("state_province")]
        public string StateProvince { get; set; }

        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("address_residential_indicator")]
        public string AddressResidentialIndicator { get; set; }
    }

    public class ReturnTo
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("company_name")]
        public string CompanyName { get; set; }

        [JsonProperty("address_line1")]
        public string AddressLine1 { get; set; }

        [JsonProperty("address_line2")]
        public object AddressLine2 { get; set; }

        [JsonProperty("address_line3")]
        public object AddressLine3 { get; set; }

        [JsonProperty("city_locality")]
        public string CityLocality { get; set; }

        [JsonProperty("state_province")]
        public string StateProvince { get; set; }

        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("address_residential_indicator")]
        public string AddressResidentialIndicator { get; set; }
    }

    public class AdvancedOptions
    {
        [JsonProperty("bill_to_account")]
        public object BillToAccount { get; set; }

        [JsonProperty("bill_to_country_code")]
        public object BillToCountryCode { get; set; }

        [JsonProperty("bill_to_party")]
        public object BillToParty { get; set; }

        [JsonProperty("bill_to_postal_code")]
        public object BillToPostalCode { get; set; }

        [JsonProperty("contains_alcohol")]
        public bool ContainsAlcohol { get; set; }

        [JsonProperty("delivered_duty_paid")]
        public bool DeliveredDutyPaid { get; set; }

        [JsonProperty("non_machinable")]
        public bool NonMachinable { get; set; }

        [JsonProperty("saturday_delivery")]
        public bool SaturdayDelivery { get; set; }

        [JsonProperty("dry_ice")]
        public bool DryIce { get; set; }

        [JsonProperty("dry_ice_weight")]
        public object DryIceWeight { get; set; }

        [JsonProperty("freight_class")]
        public object FreightClass { get; set; }

        [JsonProperty("custom_field1")]
        public object CustomField1 { get; set; }

        [JsonProperty("custom_field2")]
        public object CustomField2 { get; set; }

        [JsonProperty("custom_field3")]
        public object CustomField3 { get; set; }

        [JsonProperty("collect_on_delivery")]
        public object CollectOnDelivery { get; set; }
    }

    public class Weight
    {
        [JsonProperty("value")]
        public double Value { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }
    }

    public class Dimensions
    {
        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("length")]
        public double Length { get; set; }

        [JsonProperty("width")]
        public double Width { get; set; }

        [JsonProperty("height")]
        public double Height { get; set; }
    }

    public class InsuredValue
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }
    }

    public class LabelMessages
    {
        [JsonProperty("reference1")]
        public object Reference1 { get; set; }

        [JsonProperty("reference2")]
        public object Reference2 { get; set; }

        [JsonProperty("reference3")]
        public object Reference3 { get; set; }
    }

    public class Package
    {
        [JsonProperty("package_code")]
        public string PackageCode { get; set; }

        [JsonProperty("weight")]
        public Weight Weight { get; set; }

        [JsonProperty("dimensions")]
        public Dimensions Dimensions { get; set; }

        [JsonProperty("insured_value")]
        public InsuredValue InsuredValue { get; set; }

        [JsonProperty("label_messages")]
        public LabelMessages LabelMessages { get; set; }

        [JsonProperty("external_package_id")]
        public object ExternalPackageId { get; set; }
    }

    public class TotalWeight
    {
        [JsonProperty("value")]
        public double Value { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }
    }




}

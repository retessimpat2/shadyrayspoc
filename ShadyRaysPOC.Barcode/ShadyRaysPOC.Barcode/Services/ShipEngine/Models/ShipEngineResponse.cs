﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ShadyRaysPOC.Barcode.Services.ShipEngine.Models
{
    public abstract class ShipEngineResponse
    {
        [JsonProperty("request_id")]
        public string RequestId { get; set; }

        [JsonProperty("errors")]
        public List<ErrorModel> Errors { get; set; }

        public ShipEngineResponse() { }

        public bool HasError 
        {
            get 
            {
                return (Errors != null && Errors.Count > 0);
            }
        }

        public string Message { get; set; }
    }
}

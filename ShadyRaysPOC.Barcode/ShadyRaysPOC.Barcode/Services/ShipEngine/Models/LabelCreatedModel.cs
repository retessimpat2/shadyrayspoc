﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ShadyRaysPOC.Barcode.Services.ShipEngine.Models
{
    public class LabelCreatedModel : ShipEngineResponse
    {
        [JsonProperty("label_id")]
        public string LabelId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("shipment_id")]
        public string ShipmentId { get; set; }

        [JsonProperty("ship_date")]
        public DateTime? ShipDate { get; set; }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty("shipment_cost")]
        public ShipmentCostModel ShipmentCost { get; set; }

        [JsonProperty("insurance_cost")]
        public InsuranceCostModel InsuranceCost { get; set; }

        [JsonProperty("tracking_number")]
        public string TrackingNumber { get; set; }

        [JsonProperty("is_return_label")]
        public bool IsReturnLabel { get; set; }

        [JsonProperty("is_international")]
        public bool IsInternational { get; set; }

        [JsonProperty("batch_id")]
        public string BatchId { get; set; }

        [JsonProperty("carrier_id")]
        public string CarrierId { get; set; }

        [JsonProperty("service_code")]
        public string ServiceCode { get; set; }

        [JsonProperty("package_code")]
        public string PackageCode { get; set; }

        [JsonProperty("voided")]
        public bool Voided { get; set; }

        [JsonProperty("label_format")]
        public string LabelFormat { get; set; }

        [JsonProperty("display_scheme")]
        public string DisplayScheme { get; set; }

        [JsonProperty("label_layout")]
        public string LabelLayout { get; set; }

        [JsonProperty("trackable")]
        public bool Trackable { get; set; }

        [JsonProperty("label_image_id")]
        public object LabelImageId { get; set; }

        [JsonProperty("carrier_code")]
        public string CarrierCode { get; set; }

        [JsonProperty("tracking_status")]
        public string TrackingStatus { get; set; }

        [JsonProperty("label_download")]
        public LabelDownloadModel LabelDownload { get; set; }

        [JsonProperty("packages")]
        public List<Package> Packages { get; set; }

        [JsonProperty("charge_event")]
        public string ChargeEvent { get; set; }
    }

    public class ShipmentCostModel
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }
    }

    public class InsuranceCostModel
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }
    }

    public class LabelDownloadModel
    {
        [JsonProperty("pdf")]
        public string Pdf { get; set; }

        [JsonProperty("png")]
        public string Png { get; set; }

        [JsonProperty("zpl")]
        public string Zpl { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }
    }

    public class WeightModel
    {
        [JsonProperty("value")]
        public double Value { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }
    }

    public class DimensionsModel
    {
        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("length")]
        public double Length { get; set; }

        [JsonProperty("width")]
        public double Width { get; set; }

        [JsonProperty("height")]
        public double Height { get; set; }
    }

    public class InsuredValueModel
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }
    }

    public class LabelMessagesModel
    {
        [JsonProperty("reference1")]
        public object Reference1 { get; set; }

        [JsonProperty("reference2")]
        public object Reference2 { get; set; }

        [JsonProperty("reference3")]
        public object Reference3 { get; set; }
    }

    public class PackageModel
    {
        [JsonProperty("package_code")]
        public string PackageCode { get; set; }

        [JsonProperty("weight")]
        public Weight Weight { get; set; }

        [JsonProperty("dimensions")]
        public Dimensions Dimensions { get; set; }

        [JsonProperty("insured_value")]
        public InsuredValue InsuredValue { get; set; }

        [JsonProperty("tracking_number")]
        public string TrackingNumber { get; set; }

        [JsonProperty("label_messages")]
        public LabelMessages LabelMessages { get; set; }

        [JsonProperty("external_package_id")]
        public object ExternalPackageId { get; set; }
    }
}

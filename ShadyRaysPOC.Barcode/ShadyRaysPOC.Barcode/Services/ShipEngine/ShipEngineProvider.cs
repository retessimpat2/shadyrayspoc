﻿using Newtonsoft.Json;
using RestSharp;
using ShadyRaysPOC.Barcode.Services.ShipEngine.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Linq;

namespace ShadyRaysPOC.Barcode.Services.ShipEngine
{
    public class ShipEngineProvider : IShippingService
    {
        private readonly string API_KEY;
        private readonly string URL_PATH;
        private readonly string LABELS_BY_SHIPMENT;
        private readonly string SHIPMENTS;
        private readonly string LABELS;

        public ShipEngineProvider()
        {
            API_KEY = ConfigurationManager.AppSettings["ShipEngineAPIKey"];
            URL_PATH = ConfigurationManager.AppSettings["ShipEngineUri"];
            LABELS_BY_SHIPMENT = ConfigurationManager.AppSettings["ShipEngineLabelsByShipment"];
            SHIPMENTS = ConfigurationManager.AppSettings["ShipEngineGetShipment"];
            LABELS = ConfigurationManager.AppSettings["ShipEngineLabels"];
        }

        public async Task<Barcode.Models.ShipOrderModel> GetShipOrder(int id)
        {
            var client = new RestClient($"{URL_PATH}/{SHIPMENTS}/{Constants.Barcode.SHIP_ORDER_PREFIX}{id}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("API-Key", API_KEY);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Console.Error.WriteLine($"GetShipOrder: Something was wrong | {response.ErrorMessage}");
                return null;
            }

            Barcode.Models.ShipOrderModel result = null;
     
            var responseModel =  JsonConvert.DeserializeObject<Models.ShipOrderModel>(response.Content);

            if (responseModel != null)
                result = new Barcode.Models.ShipOrderModel { OrderNumber = responseModel.ShipmentId };            

            return result;
        }

        public async Task<string> CreateAndGetLabelPDF(int id)
        {
            var response = await CreateLabelPDF(id);
            if (response.Errors != null && response.Errors.FirstOrDefault() != null && response.Errors.FirstOrDefault()?.ErrorCode == "invalid_status")
            {
                response = await GetExistingLabelByShipment(response.Errors.FirstOrDefault()?.LabelId);
            }
            return response?.LabelDownload?.Pdf;
        }

        private async Task<LabelCreatedModel> CreateLabelPDF(int id) 
        {
            var client = new RestClient($"{URL_PATH}/{LABELS_BY_SHIPMENT}/{Constants.Barcode.SHIP_ORDER_PREFIX}{id}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("API-Key", API_KEY);
            var response = client.Execute(request);
            LabelCreatedModel responseModel = null;

            if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                responseModel = JsonConvert.DeserializeObject<LabelCreatedModel>(response.Content);
            else
            {
                responseModel = new LabelCreatedModel() 
                {
                    Errors = new List<ErrorModel>() {
                       new ErrorModel()
                       {
                           Message = response.ErrorMessage
                       }
                    }
                };
            }

            return responseModel;
        }
        private async Task<LabelCreatedModel> GetExistingLabelByShipment(string id)
        {
            var client = new RestClient($"{URL_PATH}/{LABELS}/{id}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("API-Key", API_KEY);
            var response = client.Execute(request);
            LabelCreatedModel responseModel = null;

            if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                responseModel = JsonConvert.DeserializeObject<LabelCreatedModel>(response.Content);
            else
            {
                responseModel = new LabelCreatedModel()
                {
                    Errors = new List<ErrorModel>() {
                       new ErrorModel()
                       {
                           Message = response.ErrorMessage
                       }
                    }
                };
            }

            return responseModel;
        }
    }
}

﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ShadyRaysPOC.Barcode.Services.PrintService
{
    public class PrintNodeProvider : IPrintService
    {
        private readonly string URL_PATH;
        private readonly string API_KEY;
        private readonly string PRINTERS;
        private readonly string PRINT_JOBS;


        public PrintNodeProvider() 
        {
            URL_PATH = ConfigurationManager.AppSettings["PrintNodeUri"];
            PRINT_JOBS = ConfigurationManager.AppSettings["PrintNodeSendPrint"];
            API_KEY = ConfigurationManager.AppSettings["PrintNodeAPIKey"];
            PRINTERS = ConfigurationManager.AppSettings["PrintNodeGetPrinters"];
        }

        public async Task SendPrint(string url, string shipOrderTitle, int printerId)
        {
            //var client = new RestClient("https://api.printnode.com/printjobs");
            //client.Timeout = -1;
            //var request = new RestRequest(Method.POST);
            //request.AddHeader("Content-Type", "application/json");
            //request.AddHeader("Authorization", "Basic anoxbTJQWTR3UklKVk11MW9SSV9kdlFvOUNRdDNhQ1FjM0NzLU4wbFVydzo=");
            //request.AddParameter("application/json", "{\n                \"printerId\": 69962153,\n                \"title\": \"My Test PrintJob\",\n                \"contentType\": \"pdf_uri\",\n                \"content\": \"https://api.shipengine.com/v1/downloads/10/qN5LKMWljkaA4Z_dbF5Yhw/label-34550587.pdf\",\n                \"source\": \"api documentation!\",\n                \"expireAfter\": 600\n        }", ParameterType.RequestBody);
            //IRestResponse response = client.Execute(request);
            //Console.WriteLine(response.Content);


            var client = new RestClient($"{URL_PATH}/{PRINT_JOBS}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");

            var authBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{API_KEY}:"));
            request.AddHeader("Authorization", $"Basic {authBase64}");

            request.AddParameter("application/json", JsonConvert.SerializeObject(new Models.PrintJobsRequest() 
            {
                PrinterId = printerId,
                Title = "",
                ContentType = "pdf_uri",
                Content = url,
                Source = "api documentation!",
                ExpireAfter = 600
            }), ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK && response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"GetPrinters: Something was wrong! | Error Message: {response?.ErrorMessage}");
            }
        }

        public async Task<List<Barcode.Models.PrinterModel>> GetPrinters()
        {
            var client = new RestClient($"{URL_PATH}/{PRINTERS}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            var authBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{API_KEY}:"));
            request.AddHeader("Authorization", $"Basic {authBase64}");

            IRestResponse response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK && response.StatusCode != System.Net.HttpStatusCode.Accepted)
            {
                throw new Exception($"GetPrinters: Something was wrong! | Error Message: {response?.ErrorMessage}");
            }

            return JsonConvert.DeserializeObject<List<Models.PrinterModel>>(response.Content)?
                .Select(x => new Barcode.Models.PrinterModel() 
                {
                    Id = x.Id,
                    Name = x.Name,
                    State = x.State,
                    IsDefault = x.IsDefault
                }).ToList();
        }
    }
}

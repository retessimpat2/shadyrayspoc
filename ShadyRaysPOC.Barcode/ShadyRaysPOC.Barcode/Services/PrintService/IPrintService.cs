﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShadyRaysPOC.Barcode.Services.PrintService
{
    public interface IPrintService
    {
        Task SendPrint(string url, string shipOrderTitle, int printerId);
        Task<List<Barcode.Models.PrinterModel>> GetPrinters();
    }
}

﻿using Newtonsoft.Json;

namespace ShadyRaysPOC.Barcode.Services.PrintService.Models
{
    public class PrinterModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("default")]
        public bool? IsDefault { get; set; }
    }
}

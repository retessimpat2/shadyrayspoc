﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShadyRaysPOC.Barcode.Services.PrintService.Models
{
    public class PrintJobsRequest
    {
        [JsonProperty("printerId")]
        public int PrinterId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("contentType")]
        public string ContentType { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("expireAfter")]
        public int ExpireAfter { get; set; }
    }


}

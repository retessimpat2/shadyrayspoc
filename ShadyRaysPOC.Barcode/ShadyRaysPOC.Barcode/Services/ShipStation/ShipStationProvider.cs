﻿using RestSharp;
using ShadyRaysPOC.Barcode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Newtonsoft.Json;

namespace ShadyRaysPOC.Barcode.Services.ShipStation
{
    public class ShipStationProvider : IShippingService
    {
        private readonly string  _authorization;

        public ShipStationProvider() 
        {
            _authorization = GetAuthorization();
        }

        public Task<string> CreateAndGetLabelPDF(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<ShipOrderModel> GetShipOrder(int id)
        {            
            var client = new RestClient($"{Constants.ShipStation.Path}/{Constants.ShipStation.Orders}/{id}");
            //client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", _authorization);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Console.Error.WriteLine($"GetShipOrder: Something was wrong | {response.ErrorMessage}");
                return null;
            }                

            return JsonConvert.DeserializeObject<Models.ShipOrderModel>(response.Content);
        }

        private string  GetAuthorization() 
        {
            var apiKey = ConfigurationManager.AppSettings["ShipStationApiKey"];
            var apiSecretKey = ConfigurationManager.AppSettings["ShipStationApiSecretKey"];
            var base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{apiKey}:{apiSecretKey}"));
            return $"Basic {base64}";
        }
    }
}

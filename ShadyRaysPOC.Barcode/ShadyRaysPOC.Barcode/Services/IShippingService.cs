﻿using System.Threading.Tasks;
using ShadyRaysPOC.Barcode.Models;

namespace ShadyRaysPOC.Barcode.Services
{
    public interface IShippingService
    {
        Task<ShipOrderModel> GetShipOrder(int id);
        Task<string> CreateAndGetLabelPDF(int id);
    }
}

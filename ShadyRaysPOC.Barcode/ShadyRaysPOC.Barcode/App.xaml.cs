﻿using Microsoft.Extensions.DependencyInjection;
using ShadyRaysPOC.Barcode.Features.ShipOrders;
using System.Windows;

namespace ShadyRaysPOC.Barcode
{
    public partial class App : Application
    {
        private ServiceProvider serviceProvider;
        public App()
        {
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            serviceProvider = services.BuildServiceProvider();
        }
        private void ConfigureServices(ServiceCollection services)
        {
            services.AddScoped<Services.IShippingService, Services.ShipEngine.ShipEngineProvider>();
            services.AddScoped<Services.PrintService.IPrintService, Services.PrintService.PrintNodeProvider>();
            services.AddSingleton<MainWindow>();
        }
        private void OnStartup(object sender, StartupEventArgs e)
        {
            var mainWindow = serviceProvider.GetService<MainWindow>();
            mainWindow.Show();
        }
    }
}

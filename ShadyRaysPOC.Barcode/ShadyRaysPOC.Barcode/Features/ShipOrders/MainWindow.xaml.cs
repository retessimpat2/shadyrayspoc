﻿using DesktopWPFAppLowLevelKeyboardHook;
using ShadyRaysPOC.Barcode.Services;
using ShadyRaysPOC.Barcode.Services.PrintService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ShadyRaysPOC.Barcode.Features.ShipOrders
{
    public partial class MainWindow : Window
    {
        #region PRIVATE ELEMENTS
        private const string PRINTING_LABEL = "Printing shipping label for order";
        private Image CHECK_ICON = new Image { Source = new BitmapImage(new Uri("pack://application:,,,/Resources/checkmark.png"))};

        private LowLevelKeyboardListener _listener;
        private readonly List<CancellationTokenSource> _ts;
        private string _barcodeListened = string.Empty;
        private Models.ShipOrderModel _shipOrderCache;
        private ShipOrderSelected _frmShipOrderSelected;
        private readonly IShippingService _shipService;
        private readonly IPrintService _printService;

        private int _selectedPrinter;
        #endregion

        public MainWindow(IShippingService shipStation, IPrintService printService)
        {
            InitializeComponent();
            _shipService = shipStation;
            _printService = printService;
            this._ts = new List<CancellationTokenSource>();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        #region WPF EVENTS
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _listener = new LowLevelKeyboardListener();
            _listener.OnKeyPressed += _listener_OnKeyPressed;
            //_listener.HookKeyboard();

            this.ShowInTaskbar = false;
            this.ResizeMode = ResizeMode.NoResize;
            this.Hide();
            Minimize();
            LoadPrintersList();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //_listener.UnHookKeyboard();
            e.Cancel = true;
            this.WindowState = WindowState.Minimized;
        }
        private void Window_Deactivated(object sender, EventArgs e)
        {
            Minimize();
        }
        private void Window_LostFocus(object sender, RoutedEventArgs e)
        {
            Minimize();
        }
        private void txtSOcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && ((TextBox)sender).Text.Length > 0 && _shipOrderCache == null)            
                EvaluateShipOrder();
            
        }
        #endregion

        #region PRIVATE EVENTS
        void _listener_OnKeyPressed(object sender, KeyPressedArgs e)
        {
            if (Constants.Barcode.IGNORE_KEYS.Contains(e.KeyPressed))
                return;

            this._barcodeListened += e.KeyPressed != Key.Return ? e.KeyPressedChar : null;

            if (e.KeyPressed == Key.Return && Constants.Barcode.FINISH_WITH_RETURN || !Constants.Barcode.FINISH_WITH_RETURN)
                Evaluate(this._barcodeListened);

            _ts.ForEach(x => x.Cancel());
            _ts.Clear();

            new Task(RefreshTime).Start();
        }
        #endregion

        #region PRIVATE METHODS
        private void RefreshTime()
        {
            var ts = new CancellationTokenSource();
            var ct = ts.Token;
            _ts.Add(ts);

            Thread.Sleep(Constants.Barcode.REFRESH_INTERVAL);
            if (!ct.IsCancellationRequested)
            {
                this._barcodeListened = string.Empty;
            }
        }
        private void Evaluate(string barcode)
        {
            if (barcode.ToUpper().IndexOf(Constants.Barcode.PRINT_CODE) > -1)
            {
                Maximize();
            }
            else if (Regex.Match(barcode, Constants.Barcode.SHIP_ORDER_PATTERN, RegexOptions.IgnoreCase).Success)
            {
                var order = SearchShipOrder(barcode);
                if (order != null) 
                {
                    order.Barcode = barcode;
                    _shipOrderCache = order;
                    _frmShipOrderSelected = new ShipOrderSelected(order.OrderNumber);
                    _frmShipOrderSelected.Closed += (sender, e) => 
                    {
                        _shipOrderCache = null;
                    };
                    _frmShipOrderSelected.Show();
                }                   
            }
        }

        private Models.ShipOrderModel SearchShipOrder(string barcodeEncoded) 
        {
            Models.ShipOrderModel result = null;
            int id = 0;
            int.TryParse(barcodeEncoded.Replace(Constants.Barcode.SHIP_ORDER_PREFIX, string.Empty), out id);
            result = id != 0 ? _shipService.GetShipOrder(id).GetAwaiter().GetResult() : null;

            if (result == null)
            {
                var hex = Regex.Match(barcodeEncoded, "[a-zA-Z0-9]+", RegexOptions.IgnoreCase).Value;
                id = Convert.ToInt32(hex, 16);
                result = _shipService.GetShipOrder(id).GetAwaiter().GetResult();
            }

            return result;
        }
        void Minimize()
        {
            _listener.UnHookKeyboard();
            _listener.HookKeyboard();
            this.WindowState = WindowState.Minimized;
            this.Topmost = false;
            this.Hide();

            if (_frmShipOrderSelected != null)
                _frmShipOrderSelected.Close();

            Clear();
        }
        void Maximize()
        {
            Clear();
            _listener.UnHookKeyboard();

            this.Topmost = true;
            this.Show();
            this.Focus();
            this.txtSOcode.Focus();
            this.Activate();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.WindowState = WindowState.Normal;

            if (_shipOrderCache != null)
            {
                this.txtSOcode.Text = _shipOrderCache.OrderNumber;
                PrintShipOrder(_shipOrderCache.OrderNumber);
            }
        }
        void Clear()
        {
            lblPrinting.Content = string.Empty;
            pnlPrinting.Visibility = Visibility.Hidden;
            gifPrinter.Visibility = Visibility.Hidden;
            this.txtSOcode.Text = string.Empty;
            this.txtSOcode.IsReadOnly = false;
            this.Height = 250;
        }
        private void EvaluateShipOrder()
        {
            var shipOrder = SearchShipOrder(txtSOcode.Text);
            if (shipOrder != null)
            {
                this.txtSOcode.Text = shipOrder.OrderNumber;
                PrintShipOrder(shipOrder.OrderNumber); 
            }            
            else 
            {
                lblPrinting.Content = $"No ship order number found";
                pnlPrinting.Visibility = Visibility.Visible;
            }
        }

        private void PrintShipOrder(string orderNumber) 
        {
            lblPrinting.Content = $"{PRINTING_LABEL} {orderNumber}";
            pnlPrinting.Visibility = Visibility.Visible;
            gifPrinter.Visibility = Visibility.Visible;
            this.txtSOcode.IsReadOnly = true;

            new Thread(async f =>
            {
                int.TryParse(orderNumber.Replace(Constants.Barcode.SHIP_ORDER_PREFIX, string.Empty), out int id);
                var pdfUrl = await _shipService.CreateAndGetLabelPDF(id);

                await _printService.SendPrint(pdfUrl, $"ShipOrder - {orderNumber}", _selectedPrinter);

                var form = (Window)f;
                form.Dispatcher.Invoke(() => Minimize());
            }).Start(this);
        }
        private void LoadPrintersList()
        {
            var printers = _printService.GetPrinters().GetAwaiter().GetResult();
            optSelectPrinter.Items?.Clear();
            printers.ForEach(p => 
            {
                Image icon = null;
                if (_selectedPrinter > 0 && _selectedPrinter.Equals(p.Id))
                    icon = CHECK_ICON;
                else if (p.IsDefault.HasValue && p.IsDefault.Value) 
                {
                    icon = CHECK_ICON;
                    _selectedPrinter = p.Id;
                }


                var item = new MenuItem()
                {
                    Header = $"{p.Name} ({p.State})",
                    Tag = p.Id,
                    Icon = icon
                };
                item.Click += SelectPrinter_Click;

                optSelectPrinter.Items.Add(item);
            });

        }

        private void SelectPrinter_Click(object sender, RoutedEventArgs e)
        {
            var menuItemSelected = (sender as MenuItem);
            _selectedPrinter = (int)menuItemSelected.Tag;

            foreach (MenuItem item in optSelectPrinter.Items)            
                item.Icon = _selectedPrinter.Equals((int)item.Tag) ? CHECK_ICON : null;
            
        }
        #endregion

        private void OptionOpenWindow_Click(object sender, RoutedEventArgs e)
        {
            Maximize();
        }
    }
}

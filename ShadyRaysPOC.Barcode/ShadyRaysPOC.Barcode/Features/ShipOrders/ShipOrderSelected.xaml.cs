﻿using ShadyRaysPOC.Barcode.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ShadyRaysPOC.Barcode.Features.ShipOrders
{
    /// <summary>
    /// Interaction logic for ShipOrderSelected.xaml
    /// </summary>
    public partial class ShipOrderSelected : Window
    {
        public string ShipOrder 
        {
            get 
            {
                return lblShipOrder.Content.ToString();
            }
            set 
            {
                lblShipOrder.Content = value;
            }
        }
        public ShipOrderSelected(string shipOrder)
        {
            InitializeComponent();
            this.HideMinimizeAndMaximizeButtons();
            ShipOrder = shipOrder;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.HideMinimizeAndMaximizeButtons();
            this.WindowState = WindowState.Normal;
            this.Topmost = true;
            this.Activate();
            this.ResizeMode = ResizeMode.NoResize;

            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right - this.Width;
            this.Top = desktopWorkingArea.Bottom - this.Height;

            new Task(()=> 
            {
                Thread.Sleep(Constants.ShipOrderSelected.CLOSE_TIME_SECONDS * 1000);
                this.Dispatcher.Invoke(this.Close);
            }).Start();
        }
    }
}

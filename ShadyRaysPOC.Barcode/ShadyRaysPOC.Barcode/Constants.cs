﻿using System.Windows.Input;

namespace ShadyRaysPOC.Barcode
{
    public static class Constants
    {
        public static class Barcode 
        {
            public static int REFRESH_INTERVAL = 300;
            public static string PRINT_CODE = "AAA1234567";
            public static bool FINISH_WITH_RETURN = true;
            public static string SHIP_ORDER_PATTERN = @"\^#\^[a-zA-Z0-9]+\^";
            public static Key[] IGNORE_KEYS = { Key.LeftCtrl, Key.RightCtrl, Key.LeftShift, Key.LeftShift };
            public static string SHIP_ORDER_PREFIX = "se-";
        }

        public static class Printer 
        {
            public static int CLOSE_TIME = 3000;
        }

        public static class ShipStation
        {
            public static string Path = "https://ssapi.shipstation.com";
            public static string Orders = "orders";
        }

        public static class ShipOrderSelected
        {
            public static int CLOSE_TIME_SECONDS = 60;
        }
    }
}

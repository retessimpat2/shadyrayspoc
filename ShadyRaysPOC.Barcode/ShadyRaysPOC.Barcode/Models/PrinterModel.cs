﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShadyRaysPOC.Barcode.Models
{
    public class PrinterModel
    {
        public int Id{ get; set; }
        public string Name { get; set; }
        public string State{ get; set; }
        public bool? IsDefault { get; set; }
    }
}

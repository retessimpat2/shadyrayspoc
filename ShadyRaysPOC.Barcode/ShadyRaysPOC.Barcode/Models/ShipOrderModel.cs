﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShadyRaysPOC.Barcode.Models
{
    public class ShipOrderModel
    {
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        [Newtonsoft.Json.JsonProperty()]
        public string Barcode { get; set; }

        public ShipOrderModel() { }

        public ShipOrderModel(int orderId, string orderNumber, string barcode) 
        {
            OrderId = orderId;
            OrderNumber = orderNumber;
            Barcode = barcode;
        }
    }
}
